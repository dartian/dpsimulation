﻿using System;
using Pattern_strategie.Comportement.Interface;

namespace Pattern_strategie.Comportement.Model.Parler
{
    public class ParlerColont : IParlerComportement
    {
        public string ParlerComportement()
        {
            return "J'ai créer une nouvelle ville";
        }
    }
}