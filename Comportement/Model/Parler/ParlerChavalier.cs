﻿using Pattern_strategie.Comportement.Interface;

namespace Pattern_strategie.Comportement.Model.Parler
{
    public class ParlerChavalier : IParlerComportement
    {
        public string ParlerComportement()
        {
            return "Je suis un chevalier";
        }
    }
}