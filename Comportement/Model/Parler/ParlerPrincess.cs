﻿using Pattern_strategie.Comportement.Interface;

namespace Pattern_strategie.Comportement.Model.Parler
{
    public class ParlerPrincess : IParlerComportement
    {
        public string ParlerComportement()
        {
            return "Je suis une princess";
        }
    }
}