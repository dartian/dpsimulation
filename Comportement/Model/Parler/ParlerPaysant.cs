﻿using Pattern_strategie.Comportement.Interface;

namespace Pattern_strategie.Comportement.Model.Parler
{
    public class ParlerPaysant : IParlerComportement
    {
        public string ParlerComportement()
        {
            return "J'ai créer une nouvelle fermer";
        }
    }
}