﻿using Pattern_strategie.Comportement.Interface;

namespace Pattern_strategie.Comportement.Model.Parler
{
    public class ParlerSoldat : IParlerComportement
    {
        public string ParlerComportement()
        {
            return "Je suis un soldat";
        }
    }
}