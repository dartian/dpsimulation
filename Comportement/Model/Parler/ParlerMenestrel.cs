﻿using Pattern_strategie.Comportement.Interface;

namespace Pattern_strategie.Comportement.Model.Parler
{
    public class ParlerMenestrel : IParlerComportement
    {
        public string ParlerComportement()
        {
            return "I need a Hero";
        }
    }
}