﻿using Pattern_strategie.Comportement.Interface;

namespace Pattern_strategie.Comportement.Model.Deplacer
{
    public class DeplacerVol : IDeplacerComportement
    {
        public float DeplacerComportement()
        {
            return 3;
        }
    }
}