﻿using Pattern_strategie.Comportement.Interface;

namespace Pattern_strategie.Comportement.Model.Deplacer
{
    public class DeplacerCheval : IDeplacerComportement
    {
        public float DeplacerComportement()
        {
            return 4;
        }
    }
}