﻿using Pattern_strategie.Comportement.Interface;

namespace Pattern_strategie.Comportement.Model.Deplacer
{
    public class DeplacerPied : IDeplacerComportement
    {
        public float DeplacerComportement()
        {
            return 1;
        }
    }
}