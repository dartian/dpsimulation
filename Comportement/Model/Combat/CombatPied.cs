﻿using Pattern_strategie.Comportement.Interface;

namespace Pattern_strategie.Comportement.Model.Combat
{
    public class CombatPied : ICombatComportement
    {
        public float CombatComportement()
        {
            return 1;
        }
    }
}