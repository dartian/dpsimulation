﻿using Pattern_strategie.Comportement.Interface;

namespace Pattern_strategie.Comportement.Model.Combat
{
    public class CombatArc : ICombatComportement
    {
        public float CombatComportement()
        {
            return 2;
        }
    }
}