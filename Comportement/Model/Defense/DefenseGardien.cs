﻿using Pattern_strategie.Comportement.Interface;

namespace Pattern_strategie.Comportement.Model.Defense
{
    public class DefenseGardien : IDefenseComportement
    {
        public float DefenseComportement()
        {
            return 7;
        }
    }
}