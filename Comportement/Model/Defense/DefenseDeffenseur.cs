﻿using Pattern_strategie.Comportement.Interface;

namespace Pattern_strategie.Comportement.Model.Defense
{
    public class DefenseDeffenseur : IDefenseComportement
    {
        public float DefenseComportement()
        {
            return 2;
        }
    }
}