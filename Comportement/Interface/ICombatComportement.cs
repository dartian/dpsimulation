﻿namespace Pattern_strategie.Comportement.Interface
{
    public interface ICombatComportement
    {
        float CombatComportement();
    }
}