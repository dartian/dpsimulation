﻿namespace Pattern_strategie.Comportement.Interface
{
    public interface IDeplacerComportement
    {
        float DeplacerComportement();
    }
}