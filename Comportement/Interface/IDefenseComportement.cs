﻿namespace Pattern_strategie.Comportement.Interface
{
    public interface IDefenseComportement
    {
        float DefenseComportement();
    }
}