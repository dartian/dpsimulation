﻿namespace Pattern_strategie.Comportement.Interface
{
    public interface IParlerComportement
    {
        string ParlerComportement();
    }
}