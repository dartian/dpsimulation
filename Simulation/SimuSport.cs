﻿using System;
using System.Collections.Generic;
using System.Linq;
using Pattern_strategie.Etat_major;
using Pattern_strategie.Factory;
using Pattern_strategie.Personnage.sport;
using Pattern_strategie.Plateau;

namespace Pattern_strategie.Simulation
{
    public class SimuSport : ISimulation
    {
        public PlateauDeJeu PlateauDeJeu { get; set; }
        public Dictionary<string, SujetObserver> Observers { get; set; }
        public List<Personnage.Personnage> Personnages { get; set; }
        public int score1 { get; set; }
        public int score2 { get; set; }
        string[] goal1 = new[] { "A4", "A5", "A6" };
        string[] goal2 = new[] { "J4", "J5", "J6" };
        public void Init(string nameFile)
        {
            var factoryPlateau = new FactoryPlateauMedieval();
            factoryPlateau.CreerZone(nameFile);
            PlateauDeJeu = factoryPlateau.PlateauDeJeu;

            var factoryEtatMajor = new FactoryObserveurMediaval();
            Observers = factoryEtatMajor.CreateObserver(nameFile);

            var factoryPersonnage = new FactoryPersonnageSport {Observers = Observers};
            Personnages = factoryPersonnage.CreatePersonnages(nameFile, PlateauDeJeu.Zones);
            score1 = 0;
            score2 = 0;
        }

        public void Run(int tour)
        {
            
            var random = new Random();
            var start = random.Next(0, 100);
            var i = 0;
            var teamStart = "";
            var teamEnd = "";
            if (start < 50)
            {
                teamStart = "Team 1";
                teamEnd = "Team 2";
            }
            else
            {
                teamStart = "Team 2";
                teamEnd = "Team 1";
            }
            var etat = (EtatMajor)Observers[teamStart];
            etat.Etat = eMode.Guerre;
            etat.Notify();
            Observers[teamStart] = etat;
            etat = (EtatMajor) Observers[teamEnd];
            etat.Etat = eMode.Paix;
            etat.Notify();
            Observers[teamEnd] = etat;
            i = 0;
            foreach (var personnage in Personnages)
            {
                if (start.Equals("Team 1") && personnage.Nom.Equals("T1J3"))
                {
                    var per = (Attaquant) personnage;
                    per.Ballon = true;
                }else if (start.Equals("Team 2") && personnage.Nom.Equals("T2J3"))
                {
                    var per = (Attaquant)personnage;
                    per.Ballon = true;
                }
            }
            Console.WriteLine("La {0} commence le marche", teamStart);
            while (i < tour)
            {
                action();
                Display(i);
                i++;
            }

            Display(i);
            
        }

        public void Display(int tour)
        {
            Console.WriteLine("--------------Tour {0}--------------------", tour+1);
            Console.WriteLine("Team 1 : {0} - Team 2 : {1}", score1, score2);
        }

        private void action()
        {
            foreach (var zone in PlateauDeJeu.Zones.Where(zone => zone.Value.Personnage != null))
            {
                foreach (var listAccese in zone.Value.ListAcceses)
                {
                    if (listAccese.Zone1.Personnage == null && zone.Value.Personnage.GetType() != typeof(Gardien))
                    {
                        PlateauDeJeu.MoveElement(zone.Value, listAccese.Zone1);
                        if (goal1.Contains(listAccese.Zone1.Nom))
                        {
                            score1++;
                        }
                        if (goal2.Contains(listAccese.Zone1.Nom))
                        {
                            score2++;
                        }
                        break;
                    }
                    else if (listAccese.Zone2.Personnage == null && zone.Value.Personnage.GetType() != typeof(Gardien))
                    {
                        PlateauDeJeu.MoveElement(zone.Value, listAccese.Zone2);
                        if (goal1.Contains(listAccese.Zone2.Nom))
                        {
                            score2++;
                        }
                        if (goal1.Contains(listAccese.Zone2.Nom))
                        {
                            score2++;
                        }
                        break;
                    }
                }
            }
        }
    }
}