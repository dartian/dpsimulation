﻿using System;
using System.Collections.Generic;
using System.Linq;
using Pattern_strategie.Comportement.Model.Parler;
using Pattern_strategie.Etat_major;
using Pattern_strategie.Factory;
using Pattern_strategie.Personnage.civilation;
using Pattern_strategie.Personnage.Medieval;
using Pattern_strategie.Plateau;
using Pattern_strategie.Plateau.Zone;
using Soldat = Pattern_strategie.Personnage.civilation.Soldat;

namespace Pattern_strategie.Simulation
{
    public class SimuCivi : ISimulation
    {
        public PlateauDeJeu PlateauDeJeu { get; set; }
        public Dictionary<string, SujetObserver> Observers { get; set; }
        public List<Personnage.Personnage> Personnages { get; set; }
        public List<Personnage.Personnage> TemplatePersonnages { get; set; }
        public List<IZone> ZonePeuple { get; set; }



        public void Init(string nameFile)
        {
            var factoryPlateau = new FactoryPlateauCivilisation();
            factoryPlateau.CreerZone(nameFile);
            PlateauDeJeu = factoryPlateau.PlateauDeJeu;

            var factoryEtatMajor = new FactoryObservateurCivi();
            Observers = factoryEtatMajor.CreateObserver(nameFile);

            var random = new Random();
            var positionCapital = random.Next(0, PlateauDeJeu.Zones.Count);
            var i = 0;
            foreach (var zone in PlateauDeJeu.Zones.Keys.ToList())
            {
                if (i == positionCapital)
                {
                    
                    var newZone = (ZoneCivi) PlateauDeJeu.Zones[zone];
                    newZone.EtatZone.EtatVille();
                    PlateauDeJeu.Zones[zone] = newZone;
                    foreach (var obser in Observers.Select(sujetObserver => (Capital) sujetObserver.Value))
                    {
                        obser.capital = newZone;
                    }

                }
                i++;
            }

            var factoryPersonnage = new FactoryPersonnageCivilisation {Observers = Observers};
            Personnages = factoryPersonnage.CreatePersonnages(nameFile, PlateauDeJeu.Zones);
            TemplatePersonnages = new List<Personnage.Personnage> {new Colon(null), new Paysan(null), new Soldat(null)};
            ZonePeuple = new List<IZone>();
        }

        public void Run(int tour)
        {
            var i = 0;
            while (i < tour)
            {
                CreatePersonnage();
                DeplacerPersonnage();    
                Display(i);
                i++;
            }

        }

        public void Display(int tour)
        {
            Console.WriteLine("----------------Tour n°{0}----------------", tour+1);
            foreach (var civi in ZonePeuple.Cast<ZoneCivi>())
            {
                Console.WriteLine("La zone {0} est peuplé par un {1} et la zone est {2}", civi.Nom, civi.Personnage.GetType().Name, civi.EtatZone.GetType().Name);
            }
        }

        private void CreatePersonnage()
        {
            foreach (var sujetObserver in Observers)
            {
                
                var myCapital = (Capital) sujetObserver.Value;
                var random = new Random();
                foreach (var acce in myCapital.capital.ListAcceses)
                {
                    int perso = random.Next(0, TemplatePersonnages.Count);
                    if (acce.Zone1.Personnage == null && acce.Zone1 != myCapital.capital)
                    {
                        acce.Zone1.Personnage = TemplatePersonnages[perso];
                        if (acce.Zone1.Personnage.GetType() == typeof(Paysan))
                        {
                            acce.Zone1.Personnage.ParlerComportement = new ParlerPaysant();
                        }
                        if (acce.Zone1.Personnage.GetType() == typeof(Soldat))
                        {
                            acce.Zone1.Personnage.ParlerComportement = new ParlerSoldat();
                        }
                        if (acce.Zone1.Personnage.GetType() == typeof(Colon))
                        {
                            acce.Zone1.Personnage.ParlerComportement = new ParlerColont();
                        }
                        ZonePeuple.Add(acce.Zone1);
                    }else if (acce.Zone2.Personnage == null && acce.Zone2 != myCapital.capital)
                    {
                        acce.Zone2.Personnage = TemplatePersonnages[perso];
                        if (acce.Zone2.Personnage.GetType() == typeof(Paysan))
                        {
                            acce.Zone2.Personnage.ParlerComportement = new ParlerPaysant();
                        }
                        if (acce.Zone2.Personnage.GetType() == typeof(Soldat))
                        {
                            acce.Zone2.Personnage.ParlerComportement = new ParlerSoldat();
                        }
                        if (acce.Zone2.Personnage.GetType() == typeof(Colon))
                        {
                            acce.Zone2.Personnage.ParlerComportement = new ParlerColont();
                        }
                        ZonePeuple.Add(acce.Zone2);
                    }
                }
            }
        }

        private void DeplacerPersonnage()
        {
            if (ZonePeuple.Count == 0)
            {
                return;
            }
            int i = 0;
            while (i <  ZonePeuple.Count)
            {
                foreach (var listAccese in ZonePeuple[i].ListAcceses)
                {
                    if (listAccese.Zone1.Personnage == null)
                    {
                        PlateauDeJeu.MoveElement(ZonePeuple[i], listAccese.Zone1);
                        ZonePeuple.Remove(ZonePeuple[i]);
                        ZonePeuple.Add(listAccese.Zone1);
                        break;
                    }else if(listAccese.Zone2.Personnage == null)
                    {
                        PlateauDeJeu.MoveElement(ZonePeuple[i], listAccese.Zone2);
                        ZonePeuple.Remove(ZonePeuple[i]);
                        ZonePeuple.Add(listAccese.Zone2);
                        break;
                    }
                }
                i++;
            }
        }
    }
}