﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using Pattern_strategie.decorateur;
using Pattern_strategie.Etat_major;
using Pattern_strategie.Factory;
using Pattern_strategie.Plateau;

namespace Pattern_strategie.Simulation
{
    public class SimuMedieval : ISimulation
    {
        public PlateauDeJeu PlateauDeJeu { get; set; }
        public Dictionary<string, SujetObserver> Observers { get; set; }
        public List<Personnage.Personnage> Personnages { get; set; }
        public List<Personnage.Personnage> Decorateurs { get; set; }


        public SimuMedieval()
        {
            Decorateurs = new List<Personnage.Personnage>
            {
                new PersonnageArmure(),
                new PersonnageEpee(),
                new PersonnageEpee(),
                new PersonnageEpee(),
                new PersonnageAvecCheval()
            };
        }

        public void Init(string nameFile)
        {
            var factoryPlateau = new FactoryPlateauMedieval();
            factoryPlateau.CreerZone(nameFile);
            PlateauDeJeu = factoryPlateau.PlateauDeJeu;

            var factoryEtatMajor = new FactoryObserveurMediaval();
            Observers = factoryEtatMajor.CreateObserver(nameFile);

            var factoryPersonnage = new FactoryPersonnageMedieval {Observers = Observers};
            Personnages = factoryPersonnage.CreatePersonnages(nameFile, PlateauDeJeu.Zones);

            var random = new Random();
           
            foreach (var option in Decorateurs)
            {
                var position = random.Next(0, PlateauDeJeu.Zones.Count);
                AddOption(option, position);
            }
        }

        private void AddToZone(Personnage.Personnage personnage, int position)
        {
            var i = 0;
            foreach (var zone in this.PlateauDeJeu.Zones)
            {
                if (i == position)
                {
                    zone.Value.Personnage = personnage;
                    break;
                }
                i++;
            }
        }

        public void AddOption(Personnage.Personnage option, int position)
        {
            var i = 0;
            foreach (var zone in this.PlateauDeJeu.Zones)
            {
                if (i == position)
                {
                    zone.Value.Personnage = option;
                    break;
                }
                i++;
            }
        }

        public void Run(int tour)
        {
        }

        public void Display(int tour)
        {
            
        }
    }
}