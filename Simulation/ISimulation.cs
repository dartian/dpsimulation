﻿using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using Pattern_strategie.Etat_major;
using Pattern_strategie.Plateau;

namespace Pattern_strategie.Simulation
{
    public interface ISimulation
    {
        PlateauDeJeu PlateauDeJeu { get; set; }
        Dictionary<string, SujetObserver> Observers { get; set; } 
        List<Personnage.Personnage> Personnages { get; set; } 
        void Init(string nameFile);
        void Run(int tour);
        void Display(int tour);
    }
}