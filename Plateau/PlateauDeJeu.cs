﻿using System;
using System.Collections.Generic;
using System.Linq;
using Pattern_strategie.Personnage.civilation;
using Pattern_strategie.Plateau.Zone;

namespace Pattern_strategie.Plateau
{
    public abstract class PlateauDeJeu
    {
        public Dictionary<string, IZone> Zones { get; set; }

        public PlateauDeJeu()
        {
            Zones = new Dictionary<string, IZone>();
        }


        public IZone GetZone(string nom)
        {
            return Zones[nom];
        }

        public void AddZone(IZone zone)
        {
            if (zone != null)
            {
                this.Zones.Add(zone.Nom, zone);
            }
        }

        public void RemoveZone(string nom)
        {
            Zones.Remove(nom);
        }

        public void MoveElement(IZone oldZone, IZone newZone)
        {
            var random = new Random();
            foreach (var acce in oldZone.ListAcceses)
            {
                if (acce.Zone1 == newZone)
                {
                    if (acce.Zone1.Personnage == null)
                    {
                        acce.Zone1.Personnage = oldZone.Personnage;
                        oldZone.Personnage = null;
                        int val = random.Next(0, 100);
                        if (val > 60 && acce.Zone1.GetType() == typeof (ZoneCivi))
                        {
                            changeEtat((ZoneCivi)acce.Zone1);
                            Console.WriteLine(acce.Zone1.Personnage.ParlerComportement.ParlerComportement());            
                        }
                    }
                    else
                    {
                        actionMove(oldZone, acce.Zone1);
                    }
                    break;                 
                }else if (acce.Zone2 == newZone)
                {
                    if (acce.Zone2.Personnage == null)
                    {
                        acce.Zone2.Personnage = oldZone.Personnage;
                        oldZone.Personnage = null;
                        int val = random.Next(0, 100);
                        if (val > 60 && acce.Zone1.GetType() == typeof(ZoneCivi))
                        {
                            changeEtat((ZoneCivi)acce.Zone2);
                            Console.WriteLine(acce.Zone2.Personnage.ParlerComportement.ParlerComportement());
                        }
                    }
                    else
                    {
                        actionMove(oldZone, acce.Zone2);
                    }
                    break;
                }
            }
        }

        public abstract void actionMove(IZone zone1, IZone zone2);

        public void changeEtat(ZoneCivi zone)
        {
            if (zone.Personnage.GetType() == typeof (Colon))
            {
                zone.EtatZone.EtatVille();
            }else if (zone.Personnage.GetType() == typeof (Paysan))
            {
                zone.EtatZone.EtatFerme();
            }
        }

        public void Dijkstra(string start, string end)
        {
            var previous = new Dictionary<string, IZone>();
            var distances = new Dictionary<string, int>();
            var nodes = new List<string>();

            
        }

    }
}