﻿using System;
using Pattern_strategie.Etat_major;
using Pattern_strategie.Personnage.sport;
using Pattern_strategie.Plateau.Zone;

namespace Pattern_strategie.Plateau
{
    public class PlateauDeJeuSport : PlateauDeJeu
    {
        public override void actionMove(IZone zone1, IZone zone2)
        {
            Personnage.PersonnageSport perso1 = null;
            Personnage.PersonnageSport perso2 = null;
            if (zone1.Personnage.GetType() == typeof (Attaquant))
            {
                perso1 = new Attaquant();
            }
            if (zone1.Personnage.GetType() == typeof(Defenseur))
            {
                perso1 = new Defenseur();
            }
            if (zone2.Personnage.GetType() == typeof (Attaquant))
            {
                perso2 = new Attaquant();
            }
            if (zone2.Personnage.GetType() == typeof(Defenseur))
            {
                perso2 = new Defenseur();
            }
            if (perso1.Attaque > perso2.Attaque)
            {
                if (!perso2.Ballon) return;
                perso2.Ballon = false;
                perso1.Ballon = true;
                perso1.Etat = eMode.Guerre;
                perso1.Update();
                perso2.Etat = eMode.Paix;
                perso2.Update();
            }
            else
            {
                if (!perso1.Ballon) return;
                perso1.Ballon = false;
                perso2.Ballon = true;
                perso1.Etat = eMode.Paix;
                perso1.Update();
                perso2.Etat = eMode.Guerre;
                perso2.Update();
            }
        }
    }
}