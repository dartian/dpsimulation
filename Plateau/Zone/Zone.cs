﻿using System.Collections.Generic;
using Pattern_strategie.Plateau.Acces;

namespace Pattern_strategie.Plateau.Zone
{
    public class Zone : IZone
    {
        public Personnage.Personnage Personnage { get; set; }
        
        public string Symbole { get; set; }
        public List<IAcces> ListAcceses { get; set; }
        public string Nom { get; set; }

       

        public Zone(string nom="", string symbole="", Personnage.Personnage personnage = null)
        {
            this.Symbole = symbole;
            this.Nom = nom;
            ListAcceses = new List<IAcces>();
            this.Personnage = personnage;
        }

        

        public bool IsEmpty()
        {
            return Personnage != null;
        }

        public void AddAcces(IAcces acces)
        {
            if(acces!=null)
                ListAcceses.Add(acces);
        }
    }
}   