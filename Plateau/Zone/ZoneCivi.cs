﻿using System.Collections.Generic;
using Pattern_strategie.Etats;
using Pattern_strategie.Etats.ListEtatZone;
using Pattern_strategie.Plateau.Acces;

namespace Pattern_strategie.Plateau.Zone
{
    public class ZoneCivi : IZone
    {
        public string Symbole { get; set; }
        public List<IAcces> ListAcceses { get; set; }
        public string Nom { get; set; }
        public Personnage.Personnage Personnage { get; set; }
        public EtatZone EtatZone { get; set; }

        public ZoneCivi(string nom = "", string symbole = "", Personnage.Personnage personnage = null)
        {
            this.Symbole = symbole;
            this.Nom = nom;
            ListAcceses = new List<IAcces>();
            this.Personnage = personnage;
            EtatZone = new ZoneNeutre(this);
        }

        public bool IsEmpty()
        {
            return Personnage != null;
        }

        public void AddAcces(IAcces acces)
        {
            if (acces != null)
                ListAcceses.Add(acces);
        }

        
    }
}