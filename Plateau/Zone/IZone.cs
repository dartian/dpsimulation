﻿using System.Collections.Generic;
using Pattern_strategie.Plateau.Acces;

namespace Pattern_strategie.Plateau.Zone
{
    public interface IZone
    {
        string Symbole { get; set; }
        List<IAcces> ListAcceses { get; set; }
        string Nom { get; set; }
        bool IsEmpty();
        void AddAcces(IAcces acces);
        Personnage.Personnage Personnage { get; set; }
    }
}