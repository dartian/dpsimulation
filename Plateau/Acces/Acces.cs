﻿using Pattern_strategie.Plateau.Zone;

namespace Pattern_strategie.Plateau.Acces
{
    public class Acces : IAcces
    {
        private IZone _zone1 ;
        private IZone _zone2 ;

        public IZone Zone1
        {
            get { return _zone1; }
            set
            {
                _zone1 = value;
                _zone1.AddAcces(this);  
            } 
        }

        public IZone Zone2
        {
            get { return _zone2; }
            set
            {
                _zone2 = value;
                _zone2.AddAcces(this);
            }
        }

        public float Value { get; set; }

        public Acces(IZone zone1=null , IZone zone2=null, float value=0)
        {
            Zone1 = zone1;
            Zone2 = zone2;
            Value = value;
        }
    }
}