﻿using Pattern_strategie.Plateau.Zone;

namespace Pattern_strategie.Plateau.Acces
{
    public interface IAcces
    {
        IZone Zone1 { get; set; }
       
        IZone Zone2 {get; set; }

        float Value { get; set; }

    }
}