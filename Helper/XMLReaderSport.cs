﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using Pattern_strategie.Etat_major;
using Pattern_strategie.Factory;
using Pattern_strategie.Plateau;
using Pattern_strategie.Plateau.Acces;
using Pattern_strategie.Plateau.Zone;

namespace Pattern_strategie.Helper
{
    public class XMLReaderSport : IXmlReader
    {

        public XmlDocument XmlDocument { get; set; }

        public XMLReaderSport(string nameFile,
            Dictionary<ETypeMedievalPersonnage, Personnage.Personnage> maPersonnages = null)
        {
            if (File.Exists(nameFile))
            {
                XmlDocument = new XmlDocument();
                XmlDocument.Load(nameFile);
            }
            else
            {
                Console.WriteLine("Le fichier n'existe pas");
            }
        }


        public Dictionary<string, IZone> ParseZone()
        {
            var listZones = new Dictionary<string, IZone>();
            var nom = "";
            if (XmlDocument.DocumentElement == null) return listZones;
            foreach (
                var chilNode in
                    XmlDocument.DocumentElement.ChildNodes.Cast<XmlNode>()
                        .Where(nodes => nodes.Name == "Zone")
                        .SelectMany(nodes => nodes.ChildNodes.Cast<XmlNode>()))
            {
                switch (chilNode.Name)
                {
                    case "Nom":
                        nom = chilNode.InnerText;
                        break;
                    case "Symbole":
                        var symbole = chilNode.InnerText;
                        listZones.Add(nom, new Zone(nom, symbole));
                        nom = "";
                        break;
                    default:
                        break;
                }
            }
            return listZones;
        }

        public void ParseAcces(PlateauDeJeu plateauDeJeu)
        {
            var keys = new List<string>();
            if (XmlDocument.DocumentElement == null) return;
            foreach (
                var nodes in
                    XmlDocument.DocumentElement.ChildNodes.Cast<XmlNode>()
                        .Where(nodes => nodes.Name == "Acces")
                        .SelectMany(nodes => nodes.ChildNodes.Cast<XmlNode>()))
            {
                switch (nodes.Name)
                {
                    case "zone":
                        keys.Add(nodes.InnerText);
                        break;
                    case "temps":
                        var acces = new Acces(plateauDeJeu.GetZone(keys[0]), plateauDeJeu.GetZone(keys[1]),
                            float.Parse(nodes.InnerText));
                        keys = new List<string>();
                        break;
                    default:
                        break;
                }
            }
        }



        public List<Personnage.Personnage> ParsePersonnage(IFactoryPersonnage factoryPersonnage, Dictionary<string, SujetObserver> observers, Dictionary<string, IZone> zones)
        {
            var listPersonnages = new List<Personnage.Personnage>();
            var nom = "";
            Personnage.Personnage personnage = null;
            var etatMajor = new EtatMajor();
            if (XmlDocument.DocumentElement == null) return listPersonnages;
            foreach (
                var chilNode in
                    XmlDocument.DocumentElement.ChildNodes.Cast<XmlNode>()
                        .Where(nodes => nodes.Name == "Personne")
                        .SelectMany(nodes => nodes.ChildNodes.Cast<XmlNode>()))
            {
                switch (chilNode.Name)
                {
                    case "EtatMajor":
                        etatMajor = (EtatMajor)observers[chilNode.InnerText];
                        break;
                    case "Nom":
                        nom = chilNode.InnerText;
                        break;
                    case "Type":
                        var type =
                            (ETypeSportIPersonnage)Enum.Parse(typeof(ETypeSportIPersonnage), chilNode.InnerText);
                        personnage = factoryPersonnage.CreatePersonnage(type, nom, etatMajor);
                        listPersonnages.Add(personnage);
                        nom = "";
                        break;
                    case "Zone":
                        if (zones != null)
                        {
                            zones[chilNode.InnerText].Personnage = personnage;
                        }
                        break;
                    default:
                        break;

                }
            }
            return listPersonnages;
        }

        public Dictionary<string, SujetObserver> ParseEtatMajor()
        {
            var factory = new FactoryObserveurMediaval();
            var dictionary = new Dictionary<string, SujetObserver>();
            var eM = "";
            if (XmlDocument.DocumentElement == null)
                return null;
            foreach (var childNode in XmlDocument.DocumentElement.ChildNodes.Cast<XmlNode>()
                .Where(nodes => nodes.Name == "EtatMajor")
                .SelectMany(nodes => nodes.ChildNodes.Cast<XmlNode>()))
            {
                switch (childNode.Name)
                {
                    case "name":
                        eM = childNode.InnerText;
                        break;
                    case "etat":
                        dictionary.Add(eM,
                            new EtatMajor { Etat = (eMode)Enum.Parse(typeof(eMode), childNode.InnerText, true) });
                        break;
                    default:
                        break;
                }
            }
            return dictionary;

        }
    }
}