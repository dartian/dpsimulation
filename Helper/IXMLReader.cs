﻿using System.Collections.Generic;
using System.Xml;
using Pattern_strategie.Etat_major;
using Pattern_strategie.Factory;
using Pattern_strategie.Plateau;
using Pattern_strategie.Plateau.Zone;

namespace Pattern_strategie.Helper
{
    public interface IXmlReader
    {
        XmlDocument XmlDocument { get; set; }
        Dictionary<string, IZone> ParseZone();
        void ParseAcces(PlateauDeJeu plateauDeJeu);
        List<Personnage.Personnage> ParsePersonnage(IFactoryPersonnage factoryPersonnage, Dictionary<string, SujetObserver> observers, Dictionary<string, IZone> zones);
        Dictionary<string, SujetObserver> ParseEtatMajor();
    }
}