﻿using Pattern_strategie.Comportement.Model.Deplacer;
using Pattern_strategie.Comportement.Model.Parler;
using Pattern_strategie.Etat_major;

namespace Pattern_strategie.Personnage.Medieval
{
    public class Menestrel : Personnage
    {
        public Menestrel(string nom, EtatMajor etatMajor)
        {
            this.Nom = nom;
            this.EtatMajor = etatMajor;
            this.ParlerComportement = new ParlerMenestrel();
            this.DeplacerComportement = new DeplacerPied();
        }
    }
}