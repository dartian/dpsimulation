﻿using Pattern_strategie.Comportement.Model.Deplacer;
using Pattern_strategie.Comportement.Model.Parler;
using Pattern_strategie.Etat_major;

namespace Pattern_strategie.Personnage.Medieval
{
    public class Princess : Personnage
    {
        public Princess(string nom, EtatMajor etatMajor)
        {
            this.Nom = nom;
            this.EtatMajor = etatMajor;
            this.ParlerComportement = new ParlerPrincess();
            this.DeplacerComportement = new DeplacerCarosse();
        }

        public Princess(string nom)
        {
            this.Nom = nom;
            this.ParlerComportement = new ParlerPrincess();
            this.DeplacerComportement = new DeplacerCarosse();
            this.EtatMajor = null;
        }
    }
}