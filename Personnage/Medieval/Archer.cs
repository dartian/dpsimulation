﻿using Pattern_strategie.Comportement.Model.Combat;
using Pattern_strategie.Comportement.Model.Deplacer;
using Pattern_strategie.Comportement.Model.Parler;
using Pattern_strategie.Etat_major;

namespace Pattern_strategie.Personnage.Medieval
{
    public class Archer : Personnage
    {
        public Archer(string nom, EtatMajor etatMajor)
        {
            this.EtatMajor = etatMajor;
            this.Nom = nom;
            this.ParlerComportement = new ParlerSoldat();
            this.DeplacerComportement = new DeplacerPied();
            this.CombatComportement = new CombatArc();
        }

        public string Affichage()
        {
            return this.Nom;
        }


        public float Deplacer()
        {
            return this.DeplacerComportement.DeplacerComportement();
        }
    }
}