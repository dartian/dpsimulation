﻿using Pattern_strategie.Comportement.Model.Combat;
using Pattern_strategie.Comportement.Model.Deplacer;
using Pattern_strategie.Comportement.Model.Parler;
using Pattern_strategie.Etat_major;

namespace Pattern_strategie.Personnage.Medieval
{
    public class Mage : Personnage
    {
        public Mage(string nom, EtatMajor etatMajor)
        {
            this.Nom = nom;
            this.EtatMajor = etatMajor;
            this.ParlerComportement = new ParlerMage();
            this.CombatComportement = new CombatMagie();
            this.DeplacerComportement = new DeplacerVol();
        }
    }
}