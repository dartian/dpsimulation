﻿using Pattern_strategie.Comportement.Model.Combat;
using Pattern_strategie.Comportement.Model.Deplacer;
using Pattern_strategie.Comportement.Model.Parler;
using Pattern_strategie.Etat_major;

namespace Pattern_strategie.Personnage.Medieval
{
    public class Chevalier : Personnage
    {
        public Chevalier(string nom, EtatMajor etatMajor)
        {
            this.Nom = nom;
            this.EtatMajor = etatMajor;
            this.ParlerComportement = new ParlerChavalier();
            this.CombatComportement = new CombatCheval();
            this.DeplacerComportement = new DeplacerCheval();
        }

        public string Affichage()
        {
            return this.Nom;
        }

        public float Deplacer()
        {
            return this.DeplacerComportement.DeplacerComportement();
        }
    }
}