﻿using Pattern_strategie.Comportement.Interface;
using Pattern_strategie.Etat_major;

namespace Pattern_strategie.Personnage
{
    public abstract class Personnage : IObservateur
    {
        public string Nom { get; set; }
        public IParlerComportement ParlerComportement { get; set; }
        public Etats.EtatPersonnage EtatPersonnage { get; set; }

        private ICombatComportement _combatComportement;
        private IDeplacerComportement _deplacerComportement;
        private IDefenseComportement _defenseComportement;

        public ICombatComportement CombatComportement
        {
            get { return _combatComportement; }
            set
            {
                _combatComportement = value;
                Attaque = _combatComportement.CombatComportement();
            }
        }


        public IDeplacerComportement DeplacerComportement
        {
            get { return _deplacerComportement; }
            set
            {
                _deplacerComportement = value;
                Deplacement = _deplacerComportement.DeplacerComportement();
            }
        }

        public IDefenseComportement DefenseComportement
        {
            get { return _defenseComportement; }
            set
            {
                _defenseComportement = value;
                DefenseValue = _defenseComportement.DefenseComportement();
            }
        }

        public eMode Etat { get; set; }
        public float Attaque { get; set; }
        public float DefenseValue { get; set; }
        public float Deplacement { get; set; }
        public EtatMajor EtatMajor { get; set; }
        public float Vie { get; set; }

        public Personnage()
        {
            Nom = "";
            ParlerComportement = null;
            _combatComportement= null;
            _deplacerComportement = null;
            _defenseComportement = null;
            Vie = 100;
        }

        public Personnage(string nom, EtatMajor etatMajor)
        {
            
            Nom = nom;
            EtatMajor = etatMajor;
            ParlerComportement = null;
            _combatComportement = null;
            _deplacerComportement = null;
            _defenseComportement = null;
        }

        public string Affichage()
        {
            return Nom;
        }

        public float Combatre()
        {
            return Attaque;
        }

        public string Parler()
        {
            if (ParlerComportement != null)
            {
                return ParlerComportement.ParlerComportement();
            }
            else
            {
                return null;
            }
            
        }

        public float Deplacer()
        {
            return Deplacement;
        }

        public float Defense()
        {
            return DefenseValue;
        }

        public void Update()
        {
            Etat = EtatMajor.Etat;
        }

        public void ModifyEtat(eMode eMode)
        {
            Etat = eMode;
            EtatMajor.Etat = Etat;
            EtatMajor.Notify();
        }
    }
}
