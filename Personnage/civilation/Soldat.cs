﻿using Pattern_strategie.Comportement.Model.Combat;
using Pattern_strategie.Comportement.Model.Deplacer;
using Pattern_strategie.Comportement.Model.Parler;
using Pattern_strategie.Etat_major;

namespace Pattern_strategie.Personnage.civilation
{
    public class Soldat : Personnage
    {
        public Soldat(EtatMajor capital)
        {
            Nom = "";
            EtatMajor = capital;
            this.ParlerComportement = new ParlerSoldat();
            this.DeplacerComportement = new DeplacerPied();
            this.CombatComportement = new CombatArc();
        }
    }
}