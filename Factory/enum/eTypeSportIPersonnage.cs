﻿namespace Pattern_strategie.Factory
{
    public enum ETypeSportIPersonnage
    {
        Attaquant,
        Defenseur,
        Gardien
    }
}