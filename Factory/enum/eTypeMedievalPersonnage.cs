﻿namespace Pattern_strategie.Factory
{
    public enum ETypeMedievalPersonnage
    {
        Archer,
        Chevalier,
        Mage,
        Menestrel,
        Princess,
        Soldat
    }
}