﻿using System;
using System.Collections.Generic;
using Pattern_strategie.Comportement.Model.Parler;
using Pattern_strategie.Etat_major;
using Pattern_strategie.Helper;
using Pattern_strategie.Personnage.civilation;
using Pattern_strategie.Plateau.Zone;

namespace Pattern_strategie.Factory
{
    public class FactoryPersonnageCivilisation : IFactoryPersonnage
    {

        private readonly Dictionary<ETypeCivilisationPersonnage, Personnage.Personnage> _dictionary;
        public Dictionary<string, SujetObserver> Observers { get; set; }

        public FactoryPersonnageCivilisation()
        {
            _dictionary = new Dictionary<ETypeCivilisationPersonnage, Personnage.Personnage>
            {
                {ETypeCivilisationPersonnage.Colon, new Colon(null)},
                {ETypeCivilisationPersonnage.Soldat, new Soldat(null)},
                {ETypeCivilisationPersonnage.Paysan, new Paysan(null)},
            };
        }


        public Personnage.Personnage CreatePersonnage(Enum eTypeMedievalPersonnage, string nom, EtatMajor observer)
        {
            if (_dictionary.ContainsKey((ETypeCivilisationPersonnage)eTypeMedievalPersonnage))
            {
                var personnage = _dictionary[(ETypeCivilisationPersonnage)eTypeMedievalPersonnage];
                personnage.EtatMajor = observer;
                observer.Attach(personnage);
                personnage.Nom = nom;
                if ((ETypeCivilisationPersonnage) eTypeMedievalPersonnage == ETypeCivilisationPersonnage.Paysan)
                {
                    Console.WriteLine("ok");
                    personnage.ParlerComportement = new ParlerPaysant();
                }if ((ETypeCivilisationPersonnage) eTypeMedievalPersonnage == ETypeCivilisationPersonnage.Soldat)
                {
                    personnage.ParlerComportement = new ParlerSoldat();
                }if ((ETypeCivilisationPersonnage) eTypeMedievalPersonnage == ETypeCivilisationPersonnage.Colon)
                {
                    personnage.ParlerComportement = new ParlerColont();
                }
                return personnage;
            }
            else
            {
                return null;
            }
        }

        public List<Personnage.Personnage> CreatePersonnages(string nameFile, Dictionary<string, IZone> zones)
        {
            var xmlReader = new XMLReaderCivi(nameFile);
            return xmlReader.ParsePersonnage(this, Observers, zones);
        }
    }
}