﻿using System;
using System.Collections.Generic;
using Pattern_strategie.Etat_major;
using Pattern_strategie.Helper;
using Pattern_strategie.Personnage.Medieval;
using Pattern_strategie.Plateau.Zone;


namespace Pattern_strategie.Factory
{
    public class FactoryPersonnageMedieval : IFactoryPersonnage
    {
        private readonly Dictionary<ETypeMedievalPersonnage, Personnage.Personnage> _dictionary;
        public Dictionary<string, SujetObserver> Observers { get; set; }

        public FactoryPersonnageMedieval()
        {
            _dictionary = new Dictionary<ETypeMedievalPersonnage, Personnage.Personnage>
            {
                {ETypeMedievalPersonnage.Archer, new Archer("", null)},
                {ETypeMedievalPersonnage.Chevalier, new Chevalier("", null)},
                {ETypeMedievalPersonnage.Mage, new Mage("", null)},
                {ETypeMedievalPersonnage.Menestrel, new Menestrel("", null)},
                {ETypeMedievalPersonnage.Soldat, new Soldat("", null)},
                {ETypeMedievalPersonnage.Princess, new Princess("")}
            };
        }

        public Personnage.Personnage CreatePersonnage(Enum eTypeMedievalPersonnage, string nom, EtatMajor observer)
        {
            if (_dictionary.ContainsKey((ETypeMedievalPersonnage)eTypeMedievalPersonnage))
            {
                var personnage = _dictionary[(ETypeMedievalPersonnage)eTypeMedievalPersonnage];
                if (personnage.GetType() != typeof (Princess))
                {
                    personnage.EtatMajor = observer;
                    observer.Attach(personnage);
                }
                personnage.Nom = nom;
                return personnage;
            }
            else
            {
                return null;
            }
        }

        public List<Personnage.Personnage> CreatePersonnages(string nameFile, Dictionary<string, IZone> zones)
        {
            var xmlReader = new XmlReaderMedieval(nameFile);
            return xmlReader.ParsePersonnage(this,  Observers, zones);
        }

    }
}