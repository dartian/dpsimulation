﻿using System;
using System.Collections.Generic;
using Pattern_strategie.Comportement.Model.Combat;
using Pattern_strategie.Comportement.Model.Defense;
using Pattern_strategie.Etat_major;
using Pattern_strategie.Helper;
using Pattern_strategie.Personnage.sport;
using Pattern_strategie.Plateau.Zone;

namespace Pattern_strategie.Factory
{
    public class FactoryPersonnageSport : IFactoryPersonnage
    {
        private readonly Dictionary<ETypeSportIPersonnage, Personnage.Personnage> _dictionary;
        public Dictionary<string, SujetObserver> Observers { get; set; }

        public FactoryPersonnageSport()
        {
            _dictionary = new Dictionary<ETypeSportIPersonnage, Personnage.Personnage>
            {
                {ETypeSportIPersonnage.Attaquant, new Attaquant()},
                {ETypeSportIPersonnage.Defenseur, new Defenseur()},
                {ETypeSportIPersonnage.Gardien, new Gardien()},
            };
        }

        public Personnage.Personnage CreatePersonnage(Enum eTypeMedievalPersonnage, string nom, EtatMajor observer)
        {
            if (_dictionary.ContainsKey((ETypeSportIPersonnage)eTypeMedievalPersonnage))
            {
                var personnage = _dictionary[(ETypeSportIPersonnage)eTypeMedievalPersonnage];
                personnage.EtatMajor = observer;
                observer.Attach(personnage);
                personnage.Nom = nom;
                switch ((ETypeSportIPersonnage) eTypeMedievalPersonnage)
                {
                    case ETypeSportIPersonnage.Attaquant:
                        personnage.CombatComportement = new CombatAttaquant();
                        personnage.DefenseComportement = new DeffenseAttaquant();
                        break;
                    case ETypeSportIPersonnage.Defenseur:
                        personnage.CombatComportement = new CombatDeffenseur();
                        personnage.DefenseComportement = new DefenseDeffenseur();
                        break;
                    case ETypeSportIPersonnage.Gardien:
                        personnage.DefenseComportement = new DefenseGardien();
                        break;
                    default:
                        break;
                }
                return personnage;
            }
            else
            {
                return null;
            }
        }

        public List<Personnage.Personnage> CreatePersonnages(string nameFile, Dictionary<string, IZone> zones)
        {
            var xmlReader = new XMLReaderSport(nameFile);
            return xmlReader.ParsePersonnage(this, Observers, zones);
        }
    }
}