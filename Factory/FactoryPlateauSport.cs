﻿using Pattern_strategie.Helper;
using Pattern_strategie.Plateau;

namespace Pattern_strategie.Factory
{
    public class FactoryPlateauSport : IFactoryPlateauDeJeu
    {

        public PlateauDeJeu PlateauDeJeu { get; set; }


        public FactoryPlateauSport()
        {
            PlateauDeJeu = new PlateauDeJeuSport();
        }


        public void CreerZone(string nameFile)
        {
            var xmlReader = new XmlReaderMedieval(nameFile);
            var zones = xmlReader.ParseZone();
            PlateauDeJeu.Zones = zones;
            xmlReader.ParseAcces(PlateauDeJeu);
        }
    }
}