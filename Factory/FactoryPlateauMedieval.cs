﻿using Pattern_strategie.Helper;
using Pattern_strategie.Plateau;

namespace Pattern_strategie.Factory
{
    public class FactoryPlateauMedieval : IFactoryPlateauDeJeu
    {

        public PlateauDeJeu PlateauDeJeu { get; set; }


        public FactoryPlateauMedieval()
        {
            PlateauDeJeu = new PlateauDeJeuMedieval();
        }


        public void CreerZone(string nameFile)
        {
            var xmlReader = new XmlReaderMedieval(nameFile);
            var zones = xmlReader.ParseZone();
            PlateauDeJeu.Zones = zones;
            xmlReader.ParseAcces(PlateauDeJeu);
        }
    }
}