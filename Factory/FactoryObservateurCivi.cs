﻿using System.Collections.Generic;
using Pattern_strategie.Etat_major;
using Pattern_strategie.Helper;

namespace Pattern_strategie.Factory
{
    public class FactoryObservateurCivi : IFactoryEtatMajor
    {
        public Dictionary<string, SujetObserver> CreateObserver(string nameFile)
        {
            var xmlReader = new XMLReaderCivi(nameFile);
            return xmlReader.ParseEtatMajor();
        }
    }
}