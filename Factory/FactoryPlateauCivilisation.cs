﻿using Pattern_strategie.Helper;
using Pattern_strategie.Plateau;

namespace Pattern_strategie.Factory
{
    public class FactoryPlateauCivilisation : IFactoryPlateauDeJeu
    {
        public PlateauDeJeu PlateauDeJeu { get; set; }


        public FactoryPlateauCivilisation()
        {
            PlateauDeJeu = new PlateauDeJeuCivilisation();
        }


        public void CreerZone(string nameFile)
        {
            var xmlReader = new XMLReaderCivi(nameFile);
            var zones = xmlReader.ParseZone();
            PlateauDeJeu.Zones = zones;
            xmlReader.ParseAcces(PlateauDeJeu);
        }
    }
}