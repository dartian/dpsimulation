﻿namespace Pattern_strategie.Factory
{
    public interface IFactoryPlateauDeJeu
    {
        void CreerZone(string nameFile);
    }
}