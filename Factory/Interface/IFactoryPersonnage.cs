﻿using System;
using System.Collections.Generic;
using Pattern_strategie.Etat_major;
using Pattern_strategie.Plateau.Zone;

namespace Pattern_strategie.Factory
{
    public interface IFactoryPersonnage
    {
        Personnage.Personnage CreatePersonnage(Enum eTypeMedievalPersonnage, string nom, EtatMajor observer);
        List<Personnage.Personnage> CreatePersonnages(string nameFile, Dictionary<string, IZone> zones);
    }
}