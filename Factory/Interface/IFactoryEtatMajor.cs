﻿using System.Collections.Generic;
using Pattern_strategie.Etat_major;

namespace Pattern_strategie.Factory
{
    public interface IFactoryEtatMajor
    {
        Dictionary<string, SujetObserver> CreateObserver(string nameFile);
    }
}