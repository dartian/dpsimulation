﻿using Pattern_strategie.Plateau.Zone;

namespace Pattern_strategie.Etat_major
{
    public class Capital : EtatMajor
    {
        public eMode Etat { get; set; }
        public Capital Parent { get; set; }
        public ZoneCivi capital { get; set; }

        public Capital()
        {
            this.capital = null;
            Etat = eMode.Paix;
            Parent = null;
        }

        public Capital(ZoneCivi capital)
        {
            this.capital = capital;
            Etat = eMode.Paix;
            Parent = null;
        }

        public Capital(Capital parent, ZoneCivi capital)
        {
            Etat = parent.Etat;
            Parent = parent;
            this.capital = capital;
        }

        public Capital(Capital parent, ZoneCivi capital, ZoneCivi capital1)
        {
            Etat = parent.Etat;
            Parent = parent;
            this.capital = capital1;
        }
    }
}