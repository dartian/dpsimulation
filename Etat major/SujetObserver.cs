﻿using System.Collections.Generic;
using Pattern_strategie.Plateau.Zone;

namespace Pattern_strategie.Etat_major
{
    public class SujetObserver
    {
        private readonly List<IObservateur> _observateurs = new List<IObservateur>();
        public SujetObserver Parent { get; set; }


        public SujetObserver()
        {
            this.Parent = null;
        }

        public SujetObserver(SujetObserver parent)
        {
            this.Parent = parent;
        }

        public void Attach(IObservateur obsarvateur)
        {
            this._observateurs.Add(obsarvateur);
        }

        public void Detach(IObservateur observateur)
        {
            this._observateurs.Add(observateur);
        }

        public void Notify()
        {
            foreach (var observateur in _observateurs)
            {
                observateur.Update();
            }
        }
    }
}