﻿namespace Pattern_strategie.Etat_major
{
    public interface IObservateur
    {
        void Update();
    }
}