﻿namespace Pattern_strategie.Etat_major
{
    public class EtatMajor : SujetObserver
    {
        public eMode Etat { get; set; }
        public EtatMajor Parent { get; set; }

        public EtatMajor()
        {
            Etat= eMode.Paix;
            Parent = null;
        }

        public EtatMajor(EtatMajor parent)
        {
            Etat = parent.Etat;
            Parent = parent;
        }
    }
}
