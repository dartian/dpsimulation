﻿using System.Collections.Generic;
using Pattern_strategie.Etat_major;
using Pattern_strategie.Factory;
using Pattern_strategie.Personnage.Medieval;
using Pattern_strategie.Plateau;

namespace Pattern_strategie
{
    public class SimulationJeu
    {

        private List<Personnage.Personnage> _personnages;
        private EtatMajor _etatMajor;
        private PlateauDeJeu plateau;


        public string AfficherTous()
        {
            string afficher = "";

            foreach (var personnage in _personnages)
            {
                afficher += (personnage.Affichage());
                afficher += "\n";
            }

            return afficher;

        }

        public void ChangerComposant()
        {
            
        }

        public void CreatePersonnage(IFactoryPersonnage factoryPersonnage)
        {
            
        }

        public void CreationPersonnage()
        {



            this._etatMajor = new EtatMajor();

            Archer archer = new Archer("Billy", _etatMajor);
            Soldat soldat = new Soldat("Hanz", _etatMajor);
            Mage mage = new Mage("Baltazar", _etatMajor);
            Chevalier chevalier = new Chevalier("Arthure", _etatMajor);
            Menestrel menestrel = new Menestrel("Jaksier", _etatMajor);
            Princess princess = new Princess("Zelda", _etatMajor);

            _personnages.Add(archer);
            _personnages.Add(soldat);
            _personnages.Add(mage);
            _personnages.Add(chevalier);
            _personnages.Add(menestrel);
            _personnages.Add(princess);

            this._etatMajor.Attach(archer);
            this._etatMajor.Attach(soldat);
            this._etatMajor.Attach(mage);
            this._etatMajor.Attach(chevalier);
            this._etatMajor.Attach(menestrel);
            this._etatMajor.Attach(princess);
        }

        public string EmettreSonTous()
        {
            string afficher = "";

            foreach (var personnage in _personnages)
            {
                afficher += (personnage.Parler());
                afficher += "\n";
            }

            return afficher;
        }

        public string LancerCombat()
        {
            string afficher = "";

            foreach (var personnage in _personnages)
            {
                afficher += (personnage.Combatre());
                afficher += "\n";
            }

            return afficher;
        }

        public void EnvoyerOrdreDePaix()
        {
            this._etatMajor.Etat = eMode.Paix;
            this._etatMajor.Notify();
        }

        public void EnvoyerOrdreDeGuerre()
        {
            this._etatMajor.Etat = eMode.Guerre;
            this._etatMajor.Notify();
        }

        SimulationJeu()
        {
            

            _personnages = new List<Personnage.Personnage>();
            _etatMajor = new EtatMajor();

        }
    }
}