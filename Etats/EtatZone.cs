﻿using Pattern_strategie.Plateau.Zone;

namespace Pattern_strategie.Etats
{
    public abstract class EtatZone
    {
        public ZoneCivi ZoneCivi { get; set; }

        public EtatZone(ZoneCivi zoneCivi)
        {
            ZoneCivi = zoneCivi;
        }

        public abstract void EtatFerme();
        public abstract void EtatVille();
        public abstract void EtatNeutre();
    }
}