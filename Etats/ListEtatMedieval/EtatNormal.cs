﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pattern_strategie.Etats.ListEtatMedieval
{
    public class EtatNormal : EtatPersonnage
    {
        public EtatNormal(Personnage.Personnage personnage) : base(personnage) { }

        public override void DevientNormal() { }

        public override void TombeMalade()
        {
            Console.WriteLine("Je ne me sens pas très bien");
            MonPersonnage.EtatPersonnage = new EtatMalade(MonPersonnage);
        }

        public override void SeBlesse()
        {
            Console.WriteLine("Aie ! Je vient de me blesser !");
            MonPersonnage.EtatPersonnage = new EtatBlesse(MonPersonnage);
        }

        public override void EstTuer()
        {
            Console.WriteLine("Aaargh ! Je me meurt !");
            MonPersonnage.EtatPersonnage = new EtatMort(MonPersonnage);
        }
    }
}
