﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pattern_strategie.Etats.ListEtatMedieval
{
    public class EtatMalade : EtatPersonnage
    {
        public EtatMalade(Personnage.Personnage personnage) : base(personnage) { }

        public override void DevientNormal() 
        {
            Console.WriteLine("Je retrouve la santé !");
            MonPersonnage.EtatPersonnage = new EtatNormal(MonPersonnage);
        }

        public override void TombeMalade() { }

        public override void SeBlesse()
        {
            Console.WriteLine("Je ne me suis blessé");
            MonPersonnage.EtatPersonnage = new EtatMalade(MonPersonnage);
        }

        public override void EstTuer()
        {
            Console.WriteLine("La maladie aura eu raison de moi !");
            MonPersonnage.EtatPersonnage = new EtatMort(MonPersonnage);
        }
    }
}
