﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pattern_strategie.Etats.ListEtatMedieval
{
    public class EtatMort : EtatPersonnage
    {
        public EtatMort(Personnage.Personnage personnage) : base(personnage) { }

        public override void DevientNormal() { }

        public override void TombeMalade() { }

        public override void SeBlesse() { }

        public override void EstTuer() { }
    }
}
