﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pattern_strategie.Etats.ListEtatMedieval
{
    public class EtatBlesse : EtatPersonnage
    {
        public EtatBlesse(Personnage.Personnage personnage) : base(personnage) { }

        public override void DevientNormal()
        {
            Console.WriteLine("Mes blessures guérissent !");
            MonPersonnage.EtatPersonnage = new EtatNormal(MonPersonnage);
        }

        public override void TombeMalade()
        {
            Console.WriteLine("Ma blessure s'infecte !");
            MonPersonnage.EtatPersonnage = new EtatMalade(MonPersonnage);
        }

        public override void SeBlesse() { }

        public override void EstTuer()
        {
            Console.WriteLine("C'était fini pour moi de toute façon.");
            MonPersonnage.EtatPersonnage = new EtatMort(MonPersonnage);
        }
    }
}
