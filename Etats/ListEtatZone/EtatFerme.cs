﻿using System;
using Pattern_strategie.Plateau.Zone;

namespace Pattern_strategie.Etats.ListEtatZone
{
    public class ZoneFerme : EtatZone
    {
        public ZoneFerme(ZoneCivi zoneCivi) : base(zoneCivi)
        {
        }

        public override void EtatFerme()
        {
            Console.WriteLine("La zone {0} est déjà une ferme", ZoneCivi.Nom);
        }

        public override void EtatVille()
        {
            Console.WriteLine("La zone {0} devien une vile", ZoneCivi.Nom);
            ZoneCivi.EtatZone = new ZoneVille(ZoneCivi);
        }

        public override void EtatNeutre()
        {
            Console.WriteLine("La zone {0} est déjà une ferme et ne peux pas redevenir neutre", ZoneCivi.Nom);
        }
    }
}