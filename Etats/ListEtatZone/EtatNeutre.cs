﻿using System;
using Pattern_strategie.Plateau.Zone;

namespace Pattern_strategie.Etats.ListEtatZone
{
    public class ZoneNeutre : EtatZone
    {
        public ZoneNeutre(ZoneCivi zoneCivi) : base(zoneCivi)
        {
        }

        public override void EtatFerme()
        {
            Console.WriteLine("La zone {0} devien une ferme", ZoneCivi.Nom);
            ZoneCivi.EtatZone = new ZoneFerme(ZoneCivi);
        }

        public override void EtatVille()
        {
            Console.WriteLine("La zone {0} devien une ville", ZoneCivi.Nom);
            ZoneCivi.EtatZone = new ZoneVille(ZoneCivi);
        }

        public override void EtatNeutre()
        {
            Console.WriteLine("La zone {0} est déjà neutre", ZoneCivi.Nom);
        }
    }
}