﻿using System;
using Pattern_strategie.Plateau.Zone;

namespace Pattern_strategie.Etats.ListEtatZone
{
    public class ZoneVille : EtatZone
    {
        public ZoneVille(ZoneCivi zoneCivi) : base(zoneCivi)
        {
        }

        public override void EtatFerme()
        {
            Console.WriteLine("La zone {0} est déjà une ville et ne peux pas devenir une ferme", ZoneCivi.Nom);
        }

        public override void EtatVille()
        {
            Console.WriteLine("La zone {0} est déjà une ville", ZoneCivi.Nom);
        }

        public override void EtatNeutre()
        {
            Console.WriteLine("La zone {0} est déjà une ville et ne peux pas devenir neutre", ZoneCivi.Nom);
        }
    }
}