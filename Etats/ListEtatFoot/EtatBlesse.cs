﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pattern_strategie.Etats.ListEtatFoot
{
    public class EtatBlesse : EtatPersonnage
    {
        public EtatBlesse(Personnage.Personnage personnage) : base(personnage) { }

        public override void DevientNormal()
        {
            Console.WriteLine("Ma blessure s'est rétablie, je peux courir");
            MonPersonnage.EtatPersonnage = new EtatNormal(MonPersonnage);
        }

        public override void TombeMalade() { }
        public override void SeBlesse() { }
        public override void EstTuer() { }
    }
}
