﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pattern_strategie.Personnage;

namespace Pattern_strategie.Etats
{
    public abstract class EtatPersonnage
    {
        private Personnage.Personnage monPersonnage;

        public Personnage.Personnage MonPersonnage { get; set; }

        public EtatPersonnage(Personnage.Personnage p)
        {
            MonPersonnage = p;
        }

        //Etat Medieval
        public abstract void DevientNormal();
        public abstract void TombeMalade();
        public abstract void SeBlesse();
        public abstract void EstTuer();

        //Etat foot

        //Etat village
    }
}
