﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pattern_strategie.decorateur
{
    class PersonnageAvecCheval : Personnage.Personnage
    {
        Personnage.Personnage personnageModif = null;


        public PersonnageAvecCheval()
        {
        }



        public PersonnageAvecCheval(Personnage.Personnage persoTempo)
        {

            Nom = persoTempo.Nom;
            ParlerComportement = persoTempo.ParlerComportement;
            this.CombatComportement = persoTempo.CombatComportement;
            this.DeplacerComportement = persoTempo.DeplacerComportement;
            this.DefenseComportement = persoTempo.DefenseComportement;
            EtatMajor = persoTempo.EtatMajor;

        }
    }
}
