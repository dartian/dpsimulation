﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pattern_strategie.decorateur
{
    class PersonnageEpee : Personnage.Personnage
    {

        Personnage.Personnage personnageModif = null;

        float AttaqueEpee = 10;


        public PersonnageEpee()
        {
        }

        public PersonnageEpee(Personnage.Personnage persoTempo)
        {

            Nom = persoTempo.Nom;
            ParlerComportement = persoTempo.ParlerComportement;
            this.CombatComportement = persoTempo.CombatComportement;
            this.DeplacerComportement = persoTempo.DeplacerComportement;
            this.DefenseComportement = persoTempo.DefenseComportement;
            EtatMajor = persoTempo.EtatMajor;

        }


        public string Affichage()
        {
            return "je suis : "+Nom ;
        }

        public float Combatre()
        {
            return Attaque + AttaqueEpee;
        }

        public string Parler()
        {
            if (ParlerComportement != null)
            {
                return ParlerComportement.ParlerComportement() + "et j'ai une epee en plus";
            }
            else
            {
                return null;
            }
            
        }

        public float Deplacer()
        {
            return Deplacement;
        }

        public float Defense()
        {
            return DefenseValue;
        }

        public void Update()
        {
            Etat = EtatMajor.Etat;
        }


    }

}
