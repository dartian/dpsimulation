﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pattern_strategie.decorateur
{
    class PersonnageArmure : Personnage.Personnage
    {
        Personnage.Personnage personnageModif = null;

        float protectionArmure = 10;



        
        public PersonnageArmure()
        {
        }

        public PersonnageArmure(Personnage.Personnage persoTempo)
        {

            Nom = persoTempo.Nom;
            ParlerComportement = persoTempo.ParlerComportement;
            this.CombatComportement = persoTempo.CombatComportement;
            this.DeplacerComportement = persoTempo.DeplacerComportement;
            this.DefenseComportement = persoTempo.DefenseComportement;
            EtatMajor = persoTempo.EtatMajor;

        }




        public string Affichage()
        {
            return "je suis : " + Nom;
        }

        public float Combatre()
        {
            return Attaque ;
        }

        public string Parler()
        {
            if (ParlerComportement != null)
            {
                return ParlerComportement.ParlerComportement() + "et j'ai une Armure en plus";
            }
            else
            {
                return null;
            }

        }

        public float Deplacer()
        {
            return Deplacement;
        }

        public float Defense()
        {
            return DefenseValue + protectionArmure;
        }

        public void Update()
        {
            Etat = EtatMajor.Etat;
        }



    }
}
