﻿using System;
using System.Collections.Generic;
using Pattern_strategie.Factory;
using Pattern_strategie.Helper;
using Pattern_strategie.Personnage.Medieval;
using Pattern_strategie.Simulation;

namespace Pattern_strategie
{
    class Program
    {
        static void Main(string[] args)
        {
            
            var simuMedieval = new SimuMedieval();
            simuMedieval.Init("simu1.xml");
            Console.WriteLine("Simu 1 initialisé");
            var simuSport = new SimuSport();
            simuSport.Init("simu2.xml");
            Console.WriteLine("Simu 2 initialisé");
            var simuCivi = new SimuCivi();
            simuCivi.Init("simu3.xml");
            Console.WriteLine("Simu 3 initialisé");
            Console.WriteLine("Choisisser la simulation que vous voulez lancer:");
            Console.WriteLine("1)Simulation médiéval");
            Console.WriteLine("2)Simulation de sport");
            Console.WriteLine("3)Simulation de type civilisation");
            var value = Console.ReadLine();

            Console.WriteLine("Pendant combient de tours voulez vous faire tourner la simulation?");
            var tourInput = Console.ReadLine();
            if (tourInput != null)
            {
                var tour = int.Parse(tourInput);
                switch (value)
                {
                    case "1":
                        simuMedieval.Run(tour);
                        break;
                    case "2":
                        simuSport.Run(tour);
                        break;
                    case "3":
                        simuCivi.Run(tour);
                        break;
                    default:
                        break;
                }
            }
            Console.ReadLine();
        }
    }
}